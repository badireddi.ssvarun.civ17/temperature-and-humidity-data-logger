
# Documentation for the code and API's in Interfacing DHT22 sensor


## pinMode()

<b> Description:</b>It Sets the pin as either input pin or output pin

<b> Parameters:</b>

- Pin number to which DHT22 is connected
- INPUT or OUTPUT

<b> Usage </b>: pinMode( DHT_PIN, OUTPUT)


## digitalWrite()

<b> Description:</b>This function Sets the pin either High or Low(1 or 0)

<b>Parameters:</b>

- Pin number
- HIGH or LOW

Usage: digitalWrite( DHT_PIN, LOW );


## digitalRead()


<b> Description:</b> Reads the digital value of the input

<b> Parameter:</b> Pin number

<b> Returns:</b> 1 if the pin goes High or 0 if the pin goes Low

<b> Usage:</b> digitalRead( DHT_PIN );


## delayMicroseconds()

<b> Description:</b>It Creates delay in microseconds

<b> Parameter:</b> Number of microseconds of delay

<b>Usage:</b> delayMicroseconds(1);


## printf()

<b>Description:</b>It displays the message which is passed into this function to the serial monitor

<b>Parameter:</b> string which user wants to display

<b>Usage:</b> printf( "Data not good, skip\n" );


## wiringPiSetup()


<b> Description:</b>This initialises wiringPi.This is one of the setup functions that must be called at the start of your program or your program will fail to work correctly and assumes that the calling program is going to be using the wiringPi pin numbering scheme.

<b>Returns</b>  WiringPi treats any non-zero number as HIGH, however, 0 is the only representation of LOW.

<b>Return type:</b> int


## read_dht_data()

<b>Description:</b>This is a user defined function.It reads DHT Sensor value


## initLib()

<b>Description:</b>This is use to initiate the Shunya API libraries.








