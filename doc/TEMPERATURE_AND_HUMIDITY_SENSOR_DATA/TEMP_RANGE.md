**AUTHOR :- ANURAG SINGH**

**For different Warehouse different temperature ranges basic form are:-**

**Ambient:-** Refers to the natural temperature of the warehouse

**Air conditioned:-**  Typically refers to a temperature between 56°F and 75°F. Confectionery products are a good example of a product that must avoid hot and cold extremes

**Refrigerated :-** Typically refers to a temperature between 33°F and 55°F

**Cold/Frozen:-** Typically refers to a temperature at or below 32°F


mostly all the warehouse temperature range goes to 56°F and 75°F.

**Humidity**


Optimal humidity levels in warehouses should be from 40-50% RH. Elevated humidity levels promote the growth of mould on stored items, corrosion, and rust, it creates condensation on walls, ceiling, and floors, which creates conditions suitable for pests. It can also lead to additional insurance costs for mould and mildew claims from customers.