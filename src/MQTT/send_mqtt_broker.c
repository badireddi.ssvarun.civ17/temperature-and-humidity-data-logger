/** @file send_mqtt_broker.cpp
 *  @brief send the temperature and humidity data to mqtt broker
 *
 *	This program parese the json file containing mqtt configuration details
 *  and publishes the temperature and humidity values to the mqtt broker using
 *  Shunya Interfaces API
 *
 *  @author Shriram K
 *  @bug  When compiled in Shunya OS docker container with
 *		  command: $ "g++ problem1.cxx -lshunyaInterfaces" There comes an
 *  	  ERROR: "was not declared in this scope" for shunyaInterfaces API functions
 */

/*
 *#####################################################################
 *  Initialization block
 *  ---------------------
 *  This block contains initialization code for this particular file.
 *#####################################################################
 */

/* --- Standard Includes --- */
#include <iostream>
#include <stdlib.h>
#include <string.h>


/* --- Project Includes --- */
#include <shunyaInterfaces.h>
#include "include/rapidjson/document.h"
#include "include/rapidjson/writer.h"
#include "include/rapidjson/stringbuffer.h"
#include "include/rapidjson/filereadstream.h"
#include "include/rapidjson/filewritestream.h"
#include "include/rapidjson/prettywriter.h"

/* using standard namespace */
using namespace std;
/* using rapidjson namespace */
using namespace rapidjson;

/* MACROS */
#define FILE_NAME			"config_mqtt.json"

/* --- Global Variables --- */
/* Declare a JSON document. This JSON document parsed will be stored in this variable */
Document d;

/* JSON Object that stores the mqtt configuration details in json file */
Value& mqtt_config;

/*--- Function Prototypes ---*/
void parse_file(void);
void mqtt_init(void);
void mqtt_send(float t, float h);

/*
 *#####################################################################
 *  Process block
 *  -------------
 *
 *#####################################################################
 */

/**
 *  @brief parse_file()
 *
 *  parses the mqtt config json file and stores it in document
 *
 *  @return nothing
 */

void parse_file(void)
{

	/* Open the json file in read mode */
        FILE* fp = fopen(FILE_NAME, "r");

        /* Declare read buffer */
        char readBuffer[65536];

        /* Declare stream for reading the example stream */
        FileReadStream is(fp, readBuffer, sizeof(readBuffer));

        /* Parse example.json and store it in `d` */
        d.ParseStream(is);

        /* Close the example.json file*/
        fclose(fp);

}




/**
 *  @brief mqtt_init
 *
 *  configures the mqtt object using json document
 *
 *
 *  @parameters nothing
 *
 *  @return nothing
 */
void mqtt_init(void)
{
	/* store the mqtt config details in this global json object variable */
	mqtt_config = d["mqtt"];

	/* NOTE:This variable will be used in send_mqtt() to create a
	 * new mqtt instance and publish the data
	 */

}


/**
 *  @brief mqtt_send
 *
 *  Publishes the temperature and humidity data to mqtt broker
 *
 *
 *  @aruguments temperature and humidity data
 *
 *  @return nothing
 */
void mqtt_send(float t, float h)
{
	/* Create New Instance */
	mqttObj broker1 = newMqtt(mqtt_config);
	/* Connect to broker */
	mqttConnect(&broker1) ;
	/* Publish to topic: temperature */
	mqttPublish(&broker1, "temperature","message %f", t) ;
	/* Publish to topic: humidity */
	mqttPublish(&broker1, "humidity","message %f", h) ;
	/* Close the connection */
	mqttDisconnect(&broker1) ;
}


/**
 *  @brief main function
 *
 *  main entry point of the program
 *
 *  @return 0
 */
int main()
{
	/* Dummy values to test code */
	float temp, humid;

	/* Parse the json file */
	parse_file();

	/* Initialize the json object to be used in mqtt api */
	mqtt_init();

	/* Publish the temperature and humidity data to mqtt broker */
	mqtt_send(temp, humid);

	return 0;
}
